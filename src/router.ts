import Vue from 'vue';
import Router from 'vue-router';
import firebase from 'firebase/app';
import 'firebase/auth';

import NovellaReader from '@/views/NovellaReader.vue';
import NovellasIndex from '@/views/NovellasIndex.vue';

import Login from '@/views/Login.vue';
import SignUp from '@/views/SignUp.vue';
import Home from '@/views/Home.vue';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/novellas',
      name: 'novellasIndex',
      component: NovellasIndex,
    },
    {
      path: '/novellas/:novId',
      redirect: '/novellas/:novId/fragments',
    },
    {
      path: '/novellas/:novId/fragments',
      name: 'fragments',
      component: NovellaReader,
      props: (route) => ({
        novId: route.params.novId,
        editor: route.query.editor,
      }),
    },
    {
      path: '/novellas/:novId/fragments/:fragOrder',
      name: 'fragment',
      component: NovellaReader,
      props: (route) => ({
        novId: route.params.novId,
        fragOrder: route.params.fragOrder,
        editor: route.query.editor,
      }),
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/sign-up',
      name: 'SignUp',
      component: SignUp,
    },
    {
      path: '/home',
      name: 'Home',
      component: Home,
      meta: {
        requiresAuth: true,
      },
    },
    // {
    //  path: '/about',
    //  name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
    //  component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    // },
  ],
});


router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser;
  const requiresAuth = to.matched.some((record) => record.meta.requiresAuth);

  if (requiresAuth && !currentUser) {
    next('login');
  } else {
    next();
  }
});



export default router;
