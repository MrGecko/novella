// profile/getters.ts
import {GetterTree} from 'vuex';
import {NovellaState} from './types';
import {RootState} from '../types';

export const getters: GetterTree<NovellaState, RootState> = {
    fragOrderFromId: (state) => (fragId: any) => {
        if (state.novella === undefined) {
            return -1;
        }
        return state.novella.fragments.map((f) => f._id).indexOf(fragId);
    }
    /*
    fullName(state): string {
      const {user} = state;
      const firstName = (user && user.firstName) || '';
      const lastName = (user && user.lastName) || '';
      return `${firstName} ${lastName}`;
    }
    */
};
