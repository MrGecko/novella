import {MutationTree} from 'vuex';
import {NovellaState, Novella, Fragment} from './types';
import Vue from 'vue';

export function getDefaultState(): NovellaState {
    return {
        novella: undefined,
        novellaList: [],

        fragment: undefined,

        error: undefined,
        isLoading: false,
    };
}

export const mutations: MutationTree<NovellaState> = {
    setNovella(state: NovellaState, item) {
        state.novella = !!item ? Object.assign({}, item) : undefined;
        state.error = undefined;
    },
    deleteNovella(state: NovellaState) {
        state.novella = undefined;
        state.error = undefined;
    },
    setNovellaList(state: NovellaState, novellaList) {
        state.novellaList = novellaList;
        state.error = undefined;
    },
    setFragment(state: NovellaState, item) {
        state.fragment = !!item ? Object.assign({},  item) : undefined;
        state.error = undefined;
    },
    deleteFragment(state: NovellaState) {
        state.fragment = undefined;
        state.error = undefined;
    },
    setError(state, message: string) {
        state.error = message;
    },
    setLoading(state, value: boolean) {
        state.isLoading = value;
    },
};
