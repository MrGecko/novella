import {ActionTree} from 'vuex';
import {NovellaState, Fragment} from './types';
import {RootState} from '../types';
import {api} from '@/utils/http-common';
import {ApiResponse} from 'apisauce';


export function makeNewNovellaTitle(data: any) {
    return {
        type: 'doc',
        content: [{
            attrs: {level: 2},
            type: 'heading',
            content: [{
                type: 'text',
                text: data,
            }],
        }],
    };
}

export function makeNewFragmentTitle(data: any) {
    return {
        type: 'doc',
        content: [{
            attrs: {level: 2},
            type: 'heading',
            content: [{
                type: 'text',
                text: data,
            }],
        }],
    };
}

export function makeNewFragmentContent(data: any) {
    return {
        type: 'doc',
        content: [{
            type: 'paragraph',
            content: [{
                type: 'text',
                text: !!data ? data : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum et fringilla ipsum, id venenatis quam. Nullam feugiat tempor hendrerit. Fusce posuere leo quis purus gravida interdum eget sed tortor. In eu enim quis diam iaculis ultricies. Pellentesque vel consectetur massa. Suspendisse metus tellus, vehicula vitae lacinia id, sagittis eu augue. Donec in magna gravida, dictum odio vitae, ullamcorper neque. Morbi id sem libero. Pellentesque molestie tincidunt gravida. Suspendisse potenti. Nulla convallis augue porttitor, maximus mauris id, efficitur diam. In hac habitasse platea dictumst. Duis bibendum, ligula eu rutrum blandit, est augue aliquam lorem, sit amet dignissim nulla augue nec ipsum. Phasellus posuere pharetra ante et cursus. Donec ultrices congue magna, sed semper nibh rutrum vitae. Sed cursus metus vitae ligula viverra, ut dignissim metus dapibus.\n' +
                    '\n' +
                    'Ut ut odio a tortor mattis hendrerit. Nam imperdiet dolor sit amet lobortis gravida. In vitae dapibus tortor. Vestibulum sem est, porta ut hendrerit quis, mollis sit amet lectus. Donec sed mi a felis blandit suscipit at eget enim. Morbi erat orci, pretium sit amet lorem commodo, iaculis lacinia nunc. Nullam ac dictum elit, in ultrices elit. In elementum, nulla quis convallis auctor, enim neque cursus purus, id vehicula ligula lacus vitae nisi. Sed ultrices purus sed dolor mollis, quis posuere velit vehicula. Curabitur vitae pulvinar dolor. Curabitur sodales, dolor lacinia tempor pulvinar, augue odio mollis nisi, ac dictum arcu velit ac velit. Aenean ut pellentesque neque, in euismod massa.',
            }],
        }],
    };
}

export const actions: ActionTree<NovellaState, RootState> = {
    getNovellaList({commit}): any {
        commit('setLoading', true);
        commit('setError', undefined);

        return api.get(`/novellas`)
            .then((res: ApiResponse<any>) => {
                const {ok, data} = res;
                if (ok && !!data) {
                    commit('setNovellaList', data.novellas);
                } else {
                    commit('setError', data);
                    commit('setLoading', false);
                }
                commit('setLoading', false);
            })
            .catch((error: any) => {
                commit('setError', error.message);
                commit('setLoading', false);
            });
    },
    getNovella({commit}, novellaId): any {
        commit('setLoading', true);
        commit('setError', undefined);

        return api.get(`/novellas/${novellaId}`)
            .then((res: ApiResponse<any>) => {
                const {ok, data} = res;
                if (ok && !!data) {
                    commit('setNovella', data.novella);
                } else {
                    commit('setError', data);
                    commit('setLoading', false);
                }
                commit('setLoading', false);
            })
            .catch((error: any) => {
                commit('setError', error.message);
                commit('setLoading', false);
            });
    },
    getFragment({commit}, {novellaId, fragId}): any {
        commit('setLoading', true);
        commit('setError', undefined);

        return api.get(`novellas/${novellaId}/fragments/${fragId}`)
            .then((res: ApiResponse<any>) => {
                const {ok, data} = res;
                if (ok) {
                    commit('setFragment', data.fragment);
                } else {
                    commit('setError', data);
                    commit('setLoading', false);
                }
                commit('setLoading', false);
            })
            .catch((error: any) => {
                commit('setError', error.message);
                commit('setLoading', false);
            });
    },
    clearNovella({commit}) {
        commit('setFragment', undefined);
        commit('setNovella', undefined);
        commit('setError', undefined);
    },
    addFragment({commit}, {novellaId, fragment}) {
        commit('setError', undefined);

        return api.post(`/novellas/${novellaId}/fragments`, fragment)
            .then((res: ApiResponse<any>) => {
                const {ok, data} = res;
                if (ok && !!data.fragment) {
                    // commit('setFragment', data.fragment);
                } else {
                    commit('setError', 'cannot add fragment:' + res);
                    commit('setLoading', false);
                }
                commit('setLoading', false);
            })
            .catch((error: any) => {
                commit('setError', error.message);
                commit('setLoading', false);
            });
    },
    updateFragment({commit}, {novellaId, fragId, content, title}) {
        commit('setError', undefined);

        return api.put(`/novellas/${novellaId}/fragments/${fragId}`, {content, title})
            .then((res: ApiResponse<any>) => {
                const {ok, data} = res;
                if (ok && !!data.fragment) {
                    commit('setFragment', data.fragment);
                } else {
                    commit('setError', 'cannot update fragment:' + res);
                    commit('setLoading', false);
                }
                commit('setLoading', false);
            })
            .catch((error: any) => {
                commit('setError', error.message);
                commit('setLoading', false);
            });
    },
    updateNovella({commit}, {novellaId, title, meta}) {
        commit('setError', undefined);

        return api.put(`/novellas/${novellaId}`, {meta, title})
            .then((res: ApiResponse<any>) => {
                const {ok, data} = res;
                if (ok && !!data.novella) {
                    commit('setNovella', data.novella);
                } else {
                    commit('setError', 'cannot update novella:' + res);
                    commit('setLoading', false);
                }
                commit('setLoading', false);
            })
            .catch((error: any) => {
                commit('setError', error.message);
                commit('setLoading', false);
            });
    },
    addNovella({commit}, {meta, title}) {
        return api.post(`/novellas`, {meta, title})
            .then((res: ApiResponse<any>) => {
                const {ok, data} = res;
                if (ok && !!data.novellaId) {
                    // commit('setNovella', data.novella);
                } else {
                    commit('setError', 'cannot post novella:' + res);
                    commit('setLoading', false);
                }
                commit('setLoading', false);
            })
            .catch((error: any) => {
                commit('setError', error.message);
                commit('setLoading', false);
            });
    },
    deleteNovella({commit}, novellaId) {

    },
    deleteFragment({commit}, {novellaId, fragId}) {
        commit('setError', undefined);

        return api.delete(`/novellas/${novellaId}/fragments/${fragId}`)
            .then((res: ApiResponse<any>) => {
                const {ok} = res;
                if (ok) {
                    commit('deleteFragment');
                } else {
                    commit('setError', 'cannot delete fragment:' + res);
                    commit('setLoading', false);
                }
                commit('setLoading', false);
            })
            .catch((error: any) => {
                commit('setError', error.message);
                commit('setLoading', false);
            });
    },
    setError({commit}, e) {
        commit('setError', e);
    },
    clearError({commit}) {
        commit('setError', undefined);
    },
};
