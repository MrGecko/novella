// types.ts

export interface ContextEntry {
    title: string;
    content: string;
}

export interface Fragment {
    _id?: string;
    title?: Object;
    content?: Object;

    context?: ContextEntry[];
    meta?: {
        author: null,
        date: null,
        tags: String[],
    };
}

export interface Novella {
    _id?: string;
    title: Object;
    fragments: Fragment[];
    meta: {
        author: null,
        date: null,
        tags: String[],
    };
}

export interface NovellaState {
    novella?: Novella;
    novellaList: String[];

    fragment?: Fragment;

    error?: string;
    isLoading: boolean;
}
