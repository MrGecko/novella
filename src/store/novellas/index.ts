import {Module} from 'vuex';
import {getters} from './getters';
import {actions} from './actions';
import {mutations, getDefaultState} from './mutations';
import {NovellaState} from './types';
import {RootState} from '../types';


const namespaced: boolean = true;

export const novellas: Module<NovellaState, RootState> = {
    namespaced,
    state: getDefaultState(),
    getters,
    actions,
    mutations,
};
