import Vue from 'vue';
import Vuex, {StoreOptions} from 'vuex';
import {RootState} from './types';
import {novellas} from './novellas/index';


Vue.use(Vuex);

const index: StoreOptions<RootState> = {
    state: {
        version: '1.0.0', // a simple property
    },
    modules: {
        novellas,
    },
};

export default new Vuex.Store<RootState>(index);
