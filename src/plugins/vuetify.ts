import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import 'vuetify/src/stylus/app.styl';

Vue.use(Vuetify, {
  iconfont: 'md',
  theme: {
    primary: '#ADBCA5',
    secondary: '#E2A691',
    accent: '#AB6393',
    error: '#C55F45',
  },
});
