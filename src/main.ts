
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store/index';
import './plugins/vuetify';

import firebase from 'firebase/app';

Vue.config.productionTip = false;

// Initialize Firebase

const config = {
  apiKey: process.env.VUE_APP_FIREBASE_API_KEY || '',
  authDomain: 'our-novellas.firebaseapp.com',
  databaseURL: 'https://our-novellas.firebaseio.com',
  projectId: 'our-novellas',
  storageBucket: 'our-novellas.appspot.com',
  messagingSenderId: '473265059075',
  appId: '1:473265059075:web:02c21fa1a2b13fec',
};

firebase.initializeApp(config);


new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
